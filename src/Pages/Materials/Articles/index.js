import React, { useEffect, useState } from "react";
import { makeStyles, Button, Typography, Grid, Card } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import { useTheme } from "@material-ui/core/styles";
import api from "../../../Services/api";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
    backgroundColor: theme.palette.background.dark,
    margin: theme.spacing(3),
  },
  ButtonAdd: {
    marginTop: theme.spacing(4),
  },
  card: {
    width: "330px",
    height: "250px",
  },
  gridInfos: {
    width: "330px",
    height: "250px",
    marginLeft: theme.spacing(1),
  },
  img: {
    margin: theme.spacing(1),
  },
  info: {},
  appBar: {
    boxShadow: "none",
    zIndex: theme.zIndex.drawer + 1,
  },
  link: {
    color: theme.palette.link.main,
    marginTop: theme.spacing(13),
    marginLeft: theme.spacing(4),
  },
  logo: {
    height: 25,
  },
  drawer: {
    width: 240,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 240,
    borderRight: "none",
  },
  menuIcon: {
    paddingRight: theme.spacing(5),
    paddingLeft: theme.spacing(6),
  },
  icons: {
    paddingRight: theme.spacing(5),
  },
  grow: {
    flexGrow: 1,
  },
  listItemText: {
    fontSize: 14,
  },
  listItem: {
    paddingTop: 4,
    paddingBottom: 4,
  },
  subheader: {
    textTransform: "uppercase",
  },
}));

export default function Articles() {
  const classes = useStyles();
  const [data, setDatas] = useState([]);

  useEffect(() => {
    async function loadDatas() {
      const response = await api
        .get("/materiais")
        .then((res) => {
          setDatas(res.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    loadDatas();
  }, []);

  return (
    <div>
      <Grid container>
        <Grid item xs={10}>
          <h1>Todos os Artigos</h1>
        </Grid>
        <Grid item xs={2}>
          <Button
            variant="contained"
            className={classes.ButtonAdd}
            startIcon={<AddCircleIcon />}
            href={"/Materials/AddNewMaterial"}
          >
            Adicionar novo
          </Button>
        </Grid>
      </Grid>

      <Grid container spacing={4}>
        {data.map(function (item) {
          if (item.type == "artigo") {
            return (
              <Grid item lg={4} md={4} sm={6} xs={12}>
                <Card className={classes.card}>
                  <Grid container className={classes.card}>
                    <Grid item xs={6}>
                      <img
                        style={{ width: "100%", height: "100%" }}
                        src={item.thumb}
                      />
                    </Grid>
                    <Grid item xs={5} className={classes.gridInfos}>
                      <Typography
                        style={{ fontWeight: 600 }}
                        gutterBottom
                        variant="body1"
                        color="textPrimary"
                      >
                        {item.title}
                      </Typography>
                      <Typography
                        display="block"
                        variant="body2"
                        color="textSecondary"
                      >
                        {item.author}
                      </Typography>
                      <Typography
                        display="block"
                        variant="body2"
                        color="textSecondary"
                      >
                        {item.genre}
                      </Typography>{" "}
                      <Button
                        variant="contained"
                        href={`/Materiais/${item._id}`}
                        className={classes.link}
                        onClick={() =>
                          localStorage.setItem("material", item._id)
                        }
                      >
                        Ver mais
                      </Button>
                    </Grid>
                  </Grid>
                </Card>
              </Grid>
            );
          }
        })}
      </Grid>
    </div>
  );
}

const videos = [
  {
    id: 1,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 2,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 3,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 4,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 5,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 6,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 7,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
  {
    id: 8,
    title: "Conceitos sobre React Js",
    author: "Autor do Artigo",
    genre: "Programação",
    description:
      "Opa seja bem-vindo ao curso de React Js. \nNeste curso irá descobrir o jeito mais moderno de desenvolver aplicações web e sistemas web, você vai aprender a criar aplicações completas do extremo zero e entendendo na prática como o React Js funciona. \nO Curso é para qualquer pessoa, desenvolvedores que querem crescer como programadores na área de desenvolvimento web. \nEntão mesmo que você ainda não saiba nada sobre programação e quer começar na area esse curso aqui também é pra você. Vamos aprender na pratica tudo desde o zero á configurar seu ambiente de trabalho e todo ecossistema, entender oque é o react e por que usamos ele, criar seus primeiros projetos e até aplicações completas. \nTemos como objetivo sempre passar boas praticas e métodos que são os mais usados e preparando você seja para o mercado de trabalho, ou apenas para criar seus próprios projetos.",
    details:
      "O que você aprenderá: \nVocê aprenderá criar aplicações com React Js do zero ao avançado \nAprender a criar sistemas e aplicações de forma certa. \nSistemas completos com rotas, estados isolado, Componentes \nDominar todo poder do React JS",
    thumb: "/images/artigo.jpg",
    url: "https://www.udemy.com/course/curso-reactjs/",
  },
];
